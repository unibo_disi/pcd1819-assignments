package pcd1819.ex1btasks;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import pcd1819.glue.InputFacade;
import pcd1819.glue.OutputFacade;
import pcd1819.model.ProcessingStrategy;
import pcd1819.utils.Utils;

import java.io.File;
import java.time.Duration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;
import java.util.stream.Collectors;

public class TaskExecutorContinuousProcessingStrategy extends ProcessingStrategy {

    private ForkJoinPool fjpool;
    private ObservableList<RecursiveTask<Void>> tasks;

    ListChangeListener<? super String> fileListListener;

    public TaskExecutorContinuousProcessingStrategy(InputFacade input, OutputFacade output) {
        super(input, output);
    }

    @Override
    public void start() {
        super.start();
        if(!this.active) return;

        long startTime = System.nanoTime();

        // Fork-Join pool
        fjpool = new ForkJoinPool();

        // Handler to render current results
        Consumer<Map<String,Long>> resultHandler = (Map<String,Long> result) -> {
            List<String> resList = Utils.moreFrequentWords(result, Utils.K);
            Platform.runLater( () -> {
                output.latency.setValue(Duration.ofNanos(System.nanoTime()-startTime).toMillis()+"ms");
                output.outputList.clear();
                output.outputList.addAll(resList);
            });
        };

        BlockingQueue<Map<String,Long>> blockingQueue = new LinkedBlockingQueue<>();

        fjpool.submit(new MergerTask(blockingQueue, resultHandler));

        tasks = FXCollections.observableArrayList();

        // Listener for new FJ tasks to be forked
        tasks.addListener((ListChangeListener<? super RecursiveTask<Void>>) change -> {
            while (change.next()) {
                if (change.wasAdded()) {
                    change.getAddedSubList().stream().forEach(fjpool::submit);
                }
            }
        });

        // Add tasks for current items in input set
        tasks.add(new ForkJoinContinuousFilesReader(input.inputSet.stream().map(File::new).collect(Collectors.toList()), blockingQueue));

        // Add tasks for future items in input set
        fileListListener = change -> {
            while (change.next()) {
                if (change.wasAdded()) {
                    List<String> files = (List<String>) change.getAddedSubList();
                    System.out.println("Added sublist: " + files.size());
                    fjpool.submit(() -> {
                        List<File> additions = files.stream().map(File::new).collect(Collectors.toList());
                        tasks.add(new ForkJoinContinuousFilesReader(additions, blockingQueue));
                    });
                }
            }
        };
        input.inputSet.addListener(fileListListener);
    }

    @Override
    public void stop() {
        super.stop();
        fjpool.shutdownNow();
        if(fileListListener!=null) input.inputSet.removeListener(fileListListener);
    }

    public class MergerTask implements Runnable {
        private final BlockingQueue<Map<String, Long>> queue;
        private final Consumer<Map<String, Long>> resultHandler;
        Map<String,Long> result = new HashMap<>();
        AtomicInteger k = new AtomicInteger();


        public MergerTask(BlockingQueue<Map<String,Long>> queue,
                          Consumer<Map<String,Long>> resultHandler){
            this.queue = queue;
            this.resultHandler = resultHandler;
        }

        @Override
        public void run() {
            try {
                Map<String, Long> partialRes;
                while (input.started.get() && (partialRes = queue.take()) != null) {
                    Utils.mergeMapsMutable(result, partialRes);
                    k.getAndIncrement();
                    if (k.get() == input.inputSet.size() || k.get() % Utils.FREQ_UPDATE_EACH_K_FILES == 0) {
                        resultHandler.accept(result);
                    }
                }
            } catch(Exception exc){
                Utils.reportException(exc, this);
            }
        }
    }
}
