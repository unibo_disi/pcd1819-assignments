package pcd1819.model;

import javafx.beans.value.ChangeListener;
import javafx.collections.ListChangeListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pcd1819.glue.InputFacade;
import pcd1819.glue.OutputFacade;

import java.util.List;

public abstract class ProcessingStrategy {
    protected final Logger log = LoggerFactory.getLogger(this.getClass());
    protected final InputFacade input;
    protected final OutputFacade output;
    protected boolean active;

    protected ChangeListener<Boolean> startedListener;

    public ProcessingStrategy(
            InputFacade input,
            OutputFacade output){
        this.input = input;
        this.output = output;
        this.active = true;

        startedListener = (ev,prev,next) -> {
            if(this.input.started.get()) start();
            else stop();
        };

        this.input.started.addListener(startedListener);
    }

    public void start() {
        log.debug("Start: " + this.getClass());
    }
    public void stop(){
        log.debug("Stop: " + this.getClass());
    }

    public void wire(){
        log.debug("Wire " + this.getClass());
        this.active = true;
        this.input.started.removeListener(startedListener);
        this.input.started.addListener(startedListener);
    }

    public void unwire() {
        log.debug("Unwire " + this.getClass());
        this.active = false;
        this.input.started.removeListener(startedListener);
    }
}
