package pcd1819.model;

import javafx.application.Platform;
import javafx.scene.control.Button;

public class ProcessingStatus {
    private final Button startButton;
    private final Button stopButton;
    volatile boolean computationStarted = false;

    public ProcessingStatus(Button startButton, Button stopButton){
        this.startButton = startButton;
        this.stopButton = stopButton;
        this.computationStarted = false;
    }

    public void setStarted(){
        setStatus(true);
    }

    public void setStopped(){
        setStatus(false);
    }

    public void setDone(){
        setStatus(false);
    }

    private void setStatus(boolean status){
        this.computationStarted = status;
        Platform.runLater(() -> {
            this.startButton.setDisable(status);
            this.stopButton.setDisable(!status);
        });
    }
}
