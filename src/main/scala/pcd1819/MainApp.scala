package pcd1819

import javafx.application.Application
import javafx.application.Platform
import javafx.fxml.FXMLLoader
import javafx.scene.Parent
import javafx.scene.Scene
import javafx.stage.{Stage, WindowEvent}
import java.io.IOException

class MainApp extends Application {
  @throws[IOException]
  override def start(stage: Stage): Unit = {
    val loader = new FXMLLoader(getClass.getResource("/asg2.fxml"))
    val root = loader.load.asInstanceOf[Parent]
    val scene = new Scene(root)
    stage.setOnCloseRequest((we: WindowEvent) => {
        Platform.exit()
        System.exit(0)
    })
    stage.setTitle("Assignment 2")
    stage.setScene(scene)
    stage.show()
  }
}