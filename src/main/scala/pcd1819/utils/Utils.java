package pcd1819.utils;

import javafx.collections.ObservableMap;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Utils {

    /* PARAMETERS */
    public static final long N = 3; // min num of chars per word considered
    public static final long K = 10; // num of words to show
    public static final int SPLIT_CHUNK_SIZE = 500_000;
    public static final int FREQ_UPDATE_EACH_K_FILES = 1000;

    public static Map<String,Long> countWordsInFile(String path, long n){
        try {
            String content = Files.readString(Path.of(path), Charset.forName("ISO-8859-1")); // (Java 11) In Java 8: new String(Files.readAllBytes(Paths.get(path)))
            return countWordsInTxt(content, n);
        } catch (Exception e) {
            Utils.reportException(e, Utils.class);
            return new HashMap<>();
        }
    }

    public static Map<String,Long> countWordsInTxt(String txt, long n){
        Pattern p = Pattern.compile("(\\w{"+n+",})");
        Matcher m = p.matcher(txt);
        return m.results().map(mr -> mr.group(1).toLowerCase()).collect(Collectors.groupingByConcurrent(s -> s, Collectors.counting()));
    }

    public static List<String> moreFrequentWords(Map<String, Long> words, long k){
        return words.entrySet().parallelStream()
                .sorted((v1, v2) -> v2.getValue().compareTo(v1.getValue()))
                .limit(k)
                .map(s -> s.getKey() + " (" + s.getValue() + ")")
                .collect(Collectors.toList());
    }

    public static void mergeMapsMutable(Map<String,Long> base, Map<String,Long> addition){
        addition.forEach((k,v) -> base.put(k, base.getOrDefault(k,0L)+v));
    }

    public static void reportException(Exception exc, Object obj){
        System.out.print("{{"+obj+"}}");
        exc.printStackTrace();
    }
}
