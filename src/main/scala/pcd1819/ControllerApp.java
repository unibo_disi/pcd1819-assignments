package pcd1819;

import javafx.application.Platform;
import javafx.collections.ListChangeListener;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.Pane;
import javafx.stage.DirectoryChooser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pcd1819.ex0.JavaStreamProcessingStrategy;
import pcd1819.ex1btasks.TaskExecutorContinuousProcessingStrategy;
import pcd1819.ex1tasks.TaskExecutorProcessingStrategy;
import pcd1819.ex2events.EventLoopProcessingStrategy;
import pcd1819.ex3rx.RxProcessingStrategy;
import pcd1819.glue.InputFacade;
import pcd1819.glue.OutputFacade;
import pcd1819.model.ProcessingStatus;
import pcd1819.model.ProcessingStrategy;

import java.io.File;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.stream.Collectors;

public class ControllerApp implements Initializable {
    /* GUI ELEMENTS */
    @FXML private Pane pane;
    @FXML private Button addFileButton;
    @FXML private Button startButton;
    @FXML private Button stopButton;
    @FXML private ListView<String> inputSetListView;
    @FXML private ListView<String> outputListView;
    @FXML private ListView<String> strategyChoice;
    @FXML private Label latencyLabel;

    private boolean cancellationRequested = false;

    /* UTILITIES */
    static Logger log = LoggerFactory.getLogger(ControllerApp.class);

    /* MODEL */
    ProcessingStatus status;
    ProcessingStrategy[] processingStrategies;
    InputFacade input;
    OutputFacade output;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        inputSetListView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        strategyChoice.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        strategyChoice.disableProperty().bind(startButton.disabledProperty());

        // Handle selection of sources for addition to input set
        addFileButton.setOnAction((actionEvent) -> {
            DirectoryChooser dirChooser = new DirectoryChooser();
            File dir = dirChooser.showDialog(addFileButton.getScene().getWindow());
            if(dir!=null) addFilesInDirectoryToInputSet(dir);
        });

        String startingDir = "/home/roby/repos/scafi"; // Paths.get("src").toAbsolutePath().toString();
        log.debug("Starting dir: " + startingDir);
        addFilesInDirectoryToInputSet(new File(startingDir));

        // Handle change to input set
        //inputSetListView.getItems().addListener((ListChangeListener<? super String>) (s) -> {
            // log.debug("Input set change: " + s.toString());
        //});

        // Handle start of computation
        startButton.setOnAction((actionEvent) -> {
            status.setStarted();
            cancellationRequested = false;
        });

        // Handle stop of computation
        stopButton.setOnAction((actionEvent) -> {
            cancellationRequested = true;
            status.setStopped();
            outputListView.getItems().clear();
        });

        pane.setOnKeyPressed(key -> {
            if(key.getCode()== KeyCode.DELETE) {
                Set<String> indicesToDel = inputSetListView.getSelectionModel().getSelectedItems().stream().collect(Collectors.toSet());
                inputSetListView.getItems().removeAll(indicesToDel);
            }
        });

        status = new ProcessingStatus(startButton, stopButton);


        // Wire viewmodels
        input = new InputFacade(inputSetListView.getItems(), startButton.disabledProperty());
        output = new OutputFacade(outputListView.getItems(), latencyLabel.textProperty());

        processingStrategies = new ProcessingStrategy[]{
                new JavaStreamProcessingStrategy(input, output),
                new TaskExecutorProcessingStrategy(input, output),
                new TaskExecutorContinuousProcessingStrategy(input,output),
                new EventLoopProcessingStrategy(input, output),
                new RxProcessingStrategy(input, output)
        };

        strategyChoice.getSelectionModel().getSelectedIndices().addListener((ListChangeListener<? super Integer>) (change) -> {
            outputListView.getItems().clear();
            stopButton.fire();
            for(int i=0; i<processingStrategies.length; i++){
                if(strategyChoice.getSelectionModel().isSelected(i))
                    processingStrategies[i].wire();
                else
                    processingStrategies[i].unwire();
            }
        });

        strategyChoice.getSelectionModel().select(0);
    }

    private void addFilesInDirectoryToInputSet(File dir) {
        new Thread(() -> {
            try {
                System.out.println("Getting files from: " + dir.toPath());
                List<String> fileToAdd = Files.walk(dir.toPath())
                        .filter(path -> !cancellationRequested && Files.isRegularFile(path))
                        .map(path -> path.toString())
                        .collect(Collectors.toList());
                System.out.println("Adding  " + fileToAdd.size() + " files");
                Platform.runLater(() -> inputSetListView.getItems().addAll(fileToAdd));
                cancellationRequested = false;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }).start();
    }
}
