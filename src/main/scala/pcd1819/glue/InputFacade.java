package pcd1819.glue;

import javafx.beans.value.ObservableBooleanValue;
import javafx.collections.ObservableList;

public class InputFacade {
    public final ObservableList<String> inputSet;
    public final ObservableBooleanValue started;

    public InputFacade(
            ObservableList<String> inputSet,
            ObservableBooleanValue started){
        this.inputSet = inputSet;
        this.started = started;
    }
}
