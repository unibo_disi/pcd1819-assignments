package pcd1819.ex0;

import javafx.application.Platform;
import pcd1819.glue.InputFacade;
import pcd1819.glue.OutputFacade;
import pcd1819.model.ProcessingStrategy;
import pcd1819.utils.Utils;

import java.time.Duration;
import java.util.Map;
import java.util.stream.Collectors;

public class JavaStreamProcessingStrategy extends ProcessingStrategy {

    public JavaStreamProcessingStrategy(InputFacade input, OutputFacade output) {
        super(input, output);
    }

    @Override
    public void start() {
        super.start();
        if(!this.active) return;

        long startTime = System.nanoTime();

        Thread t = new Thread(() -> {
           Map<String,Long> result =
                   input.inputSet.parallelStream()
                   .map(path -> Utils.countWordsInFile(path, Utils.N).entrySet())
                   .flatMap(s -> s.stream())
                   .collect(Collectors.toMap(
                           entry -> entry.getKey(),
                           entry -> entry.getValue(),
                           Long::sum
                   ));

            Platform.runLater(() -> {
                output.latency.setValue(Duration.ofNanos(System.nanoTime()-startTime).toMillis()+"ms");
                output.outputList.clear();
                output.outputList.addAll(Utils.moreFrequentWords(result, Utils.K));
            });
        });
        t.start();
    }

}
