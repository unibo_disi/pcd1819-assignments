package pcd1819.ex3rx;

import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.disposables.Disposable;
import io.reactivex.observables.ConnectableObservable;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.BehaviorSubject;
import io.reactivex.subjects.Subject;
import javafx.application.Platform;
import javafx.collections.ListChangeListener;
import pcd1819.glue.InputFacade;
import pcd1819.glue.OutputFacade;
import pcd1819.model.ProcessingStrategy;
import pcd1819.utils.Utils;

import java.time.Duration;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class RxProcessingStrategy extends ProcessingStrategy {
    private Disposable disposable;
    Subject<String> subject;
    private ListChangeListener<? super String> fileListListener;

    public RxProcessingStrategy(InputFacade input, OutputFacade output) {
        super(input, output);
    }

    @Override
    public void start() {
        super.start();
        if(!this.active) return;

        subject = BehaviorSubject.create();

        fileListListener = change -> {
            while (change.next()) {
                if (change.wasAdded()) change.getAddedSubList().forEach(path -> subject.onNext(path));
            }
        };
        input.inputSet.addListener(fileListListener);

        long startTime = System.nanoTime();

        disposable = subject.observeOn(Schedulers.io())
                .map(path -> Utils.countWordsInFile(path, Utils.N))
                .observeOn(Schedulers.computation())
                .scan((m1,m2) -> { Utils.mergeMapsMutable(m1,m2); return m1; })
                .throttleLast(500, TimeUnit.MILLISECONDS)
                .map(m -> Utils.moreFrequentWords(m,Utils.K))
                .subscribe(lst -> {
                    Platform.runLater(() -> {
                        output.outputList.setAll(lst);
                        output.latency.setValue(Duration.ofNanos(System.nanoTime()-startTime).toMillis()+"ms");
                    });
                });

        input.inputSet.forEach(path -> subject.onNext(path));
    }

    @Override
    public void stop() {
        super.stop();
        if(disposable != null) disposable.dispose();
        if(fileListListener!=null) input.inputSet.removeListener(fileListListener);
    }
}
