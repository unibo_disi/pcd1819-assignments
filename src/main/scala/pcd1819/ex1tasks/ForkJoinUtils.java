package pcd1819.ex1tasks;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.RecursiveTask;
import java.util.function.BiFunction;
import java.util.stream.Collectors;

public class ForkJoinUtils {
    public static <T,R> List<RecursiveTask<R>>  tasksForItems(Iterable<T> items, BiFunction<T,Integer,RecursiveTask<R>> taskGen) {
        List<RecursiveTask<R>> tasks = new LinkedList<>();
        int[] i = {0};
        items.forEach(item -> {
            i[0]++;
            RecursiveTask<R> task = taskGen.apply(item,i[0]);
            task.fork();
            tasks.add(task);
        });
        return tasks;
    }

    public static Map<String,Long> joinMaps(List<RecursiveTask<Map<String,Long>>> tasks) {
        return tasks.stream().map(task -> task.join().entrySet())
                .flatMap(s -> s.stream())
                .collect(Collectors.toMap(
                        entry -> entry.getKey(),
                        entry -> entry.getValue(),
                        Long::sum
                ));
    }

    public static <T,R> void observableListOfForkedTasksForItems(
            ObservableList<RecursiveTask<R>> tasks,
            Iterable<T> items,
            BiFunction<T,Integer,RecursiveTask<R>> taskGen) {
        int[] i = {0};
        items.forEach(item -> {
            i[0]++;
            RecursiveTask<R> task = taskGen.apply(item,i[0]);
            task.fork();
            tasks.add(task);
        });
    }
}
