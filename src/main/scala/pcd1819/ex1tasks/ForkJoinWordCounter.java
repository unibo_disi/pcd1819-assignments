package pcd1819.ex1tasks;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pcd1819.utils.Utils;

import java.util.Map;
import java.util.concurrent.RecursiveTask;

public class ForkJoinWordCounter extends RecursiveTask<Map<String,Long>> {
    private final String name;
    private String text;
    private Logger log = LoggerFactory.getLogger(this.getClass());

    public ForkJoinWordCounter(String name, String text){
        this.name = name;
        this.text = text;
    }

    @Override
    protected Map<String,Long> compute() {
        Map<String,Long> result = Utils.countWordsInTxt(text, Utils.N);
        // log.debug("Counting words on " + name + " done.");
        return result;
    }
}
