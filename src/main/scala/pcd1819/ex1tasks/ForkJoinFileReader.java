package pcd1819.ex1tasks;

import com.google.common.base.Splitter;
import pcd1819.utils.Utils;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.RecursiveTask;

public class ForkJoinFileReader extends RecursiveTask<Map<String,Long>> {
    private File file;

    public ForkJoinFileReader(File file){
        this.file = file;
    }

    @Override
    protected Map<String,Long> compute() {
        try {
            //long sizeInBytes = Files.size(file.toPath());

            String content = Files.readString(file.toPath(), Charset.forName("ISO-8859-1")); // (Java 11) In Java 8: new String(Files.readAllBytes(file.toPath()))

            List<RecursiveTask<Map<String,Long>>> tasks = ForkJoinUtils.tasksForItems(
                    Splitter.fixedLength(Utils.SPLIT_CHUNK_SIZE).split(content),
                    (chunk,k) -> new ForkJoinWordCounter(file.getName()+" (chunk: " + k + ")",chunk));

            return ForkJoinUtils.joinMaps(tasks);
        } catch (Exception e) {
            Utils.reportException(e, this);
            return new HashMap<>();
        }
    }
}
